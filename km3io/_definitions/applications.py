# -*- coding: utf-8 -*-
"""
KM3NeT Data Definitions v2.0.0-9-gbae3720
https://git.km3net.de/common/km3net-dataformat
"""

# applications
data = dict(
    APPLICATION_GENHEN=   "GENHEN"   ,
    APPLICATION_GSEAGEN=  "gSeaGen"  ,
    APPLICATION_MUPAGE=   "MUPAGE"   ,
    APPLICATION_CORSIKA=  "Corsika"  ,
    APPLICATION_KM3BUU=   "KM3BUU"   ,
    APPLICATION_KM3=      "km3"      ,
    APPLICATION_KM3SIM=   "KM3Sim"   ,
    APPLICATION_JSIRENE=  "JSirene"  ,
)
